const CONFIG = require('./filtrarPerchaConfig.js');

const { nombreDeArchivo, nombreDeOut } = CONFIG;

const fs = require('fs');

const archivo = fs.readFileSync(nombreDeArchivo);

let json = JSON.parse(archivo);

json = json.filter(
    el => el.voto !== 'A Favor',
);

console.log(JSON.stringify(json));

if (nombreDeOut) fs.writeFileSync(nombreDeOut, JSON.stringify(json));